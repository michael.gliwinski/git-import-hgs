#!/usr/bin/env bats

load "${BATS_TEST_DIRNAME}/common.bash"

setup() {
  path_add "${src_dir}"
  path_add "${mock_dir}"
}

teardown() {
  path_rm "${mock_dir}"
  path_rm "${src_dir}"

  debug_out
}


@test "git-import-hgs: it runs rsync when supplied with a valid source" {
  run git-import-hgs.sh ssh://somehost.domain.tld/path/to/src/

  (( status == 0 ))
  [[ ${output} == *"\"--recursive\""* ]]
  [[ ${output} == *'exclude *'* ]]
  [[ ${output} == *"\"somehost.domain.tld:/path/to/src/\""* ]]
}

@test "git-import-hgs: it passes port to rsync as ssh option" {
  run git-import-hgs.sh ssh://somehost.domain.tld:2222/path/

  (( status == 0 ))
  [[ ${output} == *'"--rsh"'* ]]
  [[ ${output} == *'"ssh -p 2222"'* ]]
}

@test "git-import-hgs: it ensures contents of the specified path are imported" {
  # if source doesn't end with '/' rsync would normally fetch the
  # directory itself, but in case of import we want to get contents
  run git-import-hgs.sh ssh://somehost.domain.tld/path/to/src

  (( status == 0 ))
  [[ ${output} == *'"somehost.domain.tld:/path/to/src/"'* ]]
}

@test "git-import-hgs: it excludes editor .swp files" {
  # most common cause for insignificant read errors
  run git-import-hgs.sh ssh://somehost.domain.tld/path/to/src/

  (( status == 0 ))
  [[ ${output} == *"\"--exclude=*.swp\""* ]]
}

@test "git-import-hgs: it passes options specified on CLI to rsync" {
  run git-import-hgs.sh --dry-run ssh://somehost.domain.tld/path/to/src/
  
  (( status == 0 ))
  [[ ${output} == *"\"--dry-run\""* ]]
}

@test "git-import-hgs: it fails when no args specified" {
  run git-import-hgs.sh

  (( status == 2 ))
  [[ ${output} == *Usage:* ]]
}

@test "git-import-hgs: it fails when invalid args specified" {
  run git-import-hgs.sh --foo

  (( status == 1 ))
  [[ ${output} == *"not a valid URL"* ]]
}
