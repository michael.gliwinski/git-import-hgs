#!/bin/bash

mock_dir="${BATS_TEST_DIRNAME}/mock"
src_dir="${BATS_TEST_DIRNAME%/*}"  # parent dir

path_add() { # DIR
  if [[ ${PATH} == *${1}:* ]]; then
    return 0
  fi
  export PATH="${1}${PATH:+:$PATH}"
}

path_rm() { # DIR
  if [[ ${PATH} != *${1}:* ]]; then
    return 0
  fi
  export PATH="${PATH/${1}:/}"
}

debug_out() {
  {
    #echo "status: ${status}"  # status seems to be reset before teardown
    echo "output: '${output}'"
    echo "lines=("
    for i in ${!lines[@]}; do
      printf '  [%d]="%s"\n' ${i} "${lines[i]}"
    done
    echo ")"
  } >&2
}

teardown() {
  debug_out
}
